import { Component, OnInit } from '@angular/core';
import { Time } from '@angular/common';

import { Contest } from './data/contest';
import { ContestService } from './services/contest.service';
import { Participant } from './data/participant';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  contest: Contest;
  participants: Participant[];

  constructor(private contestService: ContestService) {
  }

  ngOnInit(): void {
    this.getContest();
    this.getParticipants();
  }

  getContest() {
    this.contestService.getContest().subscribe(_contest => {
      this.contest = _contest;
      this.contest.startTime = new Date(this.contest.startTime);
    });
  }

  getParticipants(): void {
    this.contestService.getParticipants().subscribe(participants => {
      this.participants = participants;
    });
  }

  getTimeToStart(): Date {
    return new Date(this.contest.startTime.valueOf() - (new Date(Date.now()).valueOf()));
  }

  getElapsedSeconds(): number {
    let startTime = this.contest.startTime;
    let curTime = new Date(Date.now());
    return (curTime.valueOf() - startTime.valueOf()) / 1000;
  }

  getRunningTime(): Time {
    let time = Math.trunc((Date.now() - this.contest.startTime.valueOf()) / 1000 / 60);
    return {
      hours: Math.trunc(time / 60),
      minutes: time % 60
    };
  }

  isRunning(): boolean {
    let elapsed = this.getElapsedSeconds();
    if (elapsed < 0)
      return false;
    return elapsed < this.contest.duration*60;
  }

  isFinished(): boolean {
    let elapsed = this.getElapsedSeconds();
    return elapsed >= this.contest.duration*60;
  }

  getParticipant() {
    if (this.participants === undefined) {
      return undefined;
    } else {
      return this.participants[0];
    }
  }
}
