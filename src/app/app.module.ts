import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { HighlightJsModule, HighlightJsService } from 'angular2-highlight-js';

import { AppComponent } from './app.component';
import { ProblemComponent } from './components/problem/problem.component';
import { MonitorComponent } from './components/monitor/monitor.component';
import { SubmissionsComponent } from './components/submissions/submissions.component';
import { ContestService } from './services/contest.service';
import { SubmissionsService } from './services/submissions.service';


@NgModule({
  declarations: [
    AppComponent,
    ProblemComponent,
    MonitorComponent,
    SubmissionsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    ProgressbarModule.forRoot(),
    HighlightJsModule,

    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, { dataEncapsulation: false })
  ],
  providers: [
    ContestService,
    SubmissionsService,
    HighlightJsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
