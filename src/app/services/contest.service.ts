import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ProblemDescription } from '../data/problem-description';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Contest } from '../data/contest';
import { Participant } from '../data/participant';

@Injectable()
export class ContestService {
  private urlContests = "api/contests";
  private urlParticipants = "api/participants";

  constructor(private http: HttpClient) { }

  getContest(id: Number = 1): Observable<Contest> {
    return this.http.get<Contest>(this.urlContests + `/${id}`);
  }

  getParticipants(): Observable<Participant[]> {
    return this.http.get<Participant[]>(this.urlParticipants);
  }
}
