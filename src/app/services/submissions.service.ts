import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Submission } from '../data/submission';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SubmissionsService {
  private url = "api/submissions";

  constructor(private http: HttpClient) { }

  getSubmissions(): Observable<Submission[]> {
    return this.http.get<Submission[]>(this.url);
  }
}
