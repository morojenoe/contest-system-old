import { InMemoryDbService } from 'angular-in-memory-web-api';
import { ProblemDescription } from './data/problem-description';
import { ProblemTest } from './data/problem-test';
import { Submission } from './data/submission';
import { Contest } from './data/contest';
import { Participant } from './data/participant';
import { ParticipantProblem } from './data/participant-problem';

class RNG {
  private seed:number;

  constructor(seed:number) {
      this.seed = seed;
  }

  private next(min:number, max:number):number {
      max = max || 0;
      min = min || 0;

      this.seed = (this.seed * 9301 + 49297) % 233280;
      var rnd = this.seed / 233280;

      return min + rnd * (max - min);
  }

  // http://indiegamr.com/generate-repeatable-random-numbers-in-js/
  public nextInt(min:number, max:number):number {
      return Math.round(this.next(min, max));
  }

  public nextDouble():number {
      return this.next(0, 1);
  }

  public pick(collection:any[]):any {
      return collection[this.nextInt(0, collection.length - 1)];
  }
};

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const problems: ProblemDescription[] = [
      {
        id: 1,
        title: "Parity",
        text: `Now and then you play the following game with your friend. Your friend writes down a sequence consisting of zeroes and ones. You choose a continuous subsequence (for example the subsequence from the third to the fifth digit inclusively) and ask him, whether this subsequence contains even or odd number of ones. Your friend answers your question and you can ask him about another subsequence and so on.
        Your task is to guess the entire sequence of numbers. You suspect some of your friend's answers may not be correct and you want to convict him of falsehood. Thus you have decided to write a program to help you in this matter. The program will receive a series of your questions together with the answers you have received from your friend. The aim of this program is to find the first answer which is provably wrong, i.e. that there exists a sequence satisfying answers to all the previous questions, but no such sequence satisfies this answer.
        `,
        inputDescription: "Input contains a series of tests. The first line of each test contains one number, which is the length of the sequence of zeroes and ones. This length is less or equal to 109. In the second line, there is one non-negative integer which is the number of questions asked and answers to them. The number of questions and answers is less or equal to 5 000. The remaining lines specify questions and answers. Each line contains one question and the answer to this question: two integers (the position of the first and last digit in the chosen subsequence) and one word which is either “even” or “odd” (the answer, i.e. the parity of the number of ones in the chosen subsequence, where “even” means an even number of ones and “odd” means an odd number). The input is ended with a line containing −1.",
        outputDescription: "Each line of output containing one integer X. Number X says that there exists a sequence of zeroes and ones satisfying first X parity conditions, but there exists none satisfying X + 1 conditions. If there exists a sequence of zeroes and ones satisfying all the given conditions, then number X should be the number of all the questions asked.",
        inputTests: [
          {
            "lines": [
              "10",
              "5",
              "1 2 even",
              "3 4 odd",
              "5 6 even",
              "1 6 even",
              "7 10 odd",
              "-1"
          ]}
        ],
        outputTests: [
          {
            "lines":[
              "3"
            ]}
        ]
      }, 
      {
        id: 2,
        title: "Bishop and Pawn",
        text: "There are a white bishop and black pawn on a chessboard. Moves are made in accordance with the usual chess rules. White moves first. Black wins if he can promote his pawn to a queen and the white bishop cannot capture the queen by the subsequent move. The game ends in a draw if it’s Black’s turn to move but the pawn cannot move forward. In other cases, White wins. It is required to tell the result of the game if both sides play optimally.",
        inputDescription: "The first and second lines show the positions of the white bishop and black pawn, respectively, by means of the standard chess notation. The rank in which the pawn is initially positioned may have the number from 2 to 7, and the bishop is initially positioned at any square different from the pawn’s square.",
        outputDescription: "Output WHITE if White wins, DRAW in the case of a draw, and BLACK if Black wins.",
        inputTests: [
          {
            "lines":[
              "a1",
              "c2"
            ]
          }
        ],
        outputTests: [
          {
            "lines": [
              "WHITE"
            ]
          }
        ]
      },
      {
        id: 3,
        title: "Phone Numbers",
        text: "",
        inputDescription: "",
        outputDescription: "",
        inputTests: [],
        outputTests: []
      },
      {
        id: 4,
        title: "Questions",
        text: "",
        inputDescription: "",
        outputDescription: "",
        inputTests: [],
        outputTests: []
      },
      {
        id: 5,
        title: "In the Army Now",
        text: "",
        inputDescription: "",
        outputDescription: "",
        inputTests: [],
        outputTests: []
      },{
        id: 6,
        title: "Transversal",
        text: "",
        inputDescription: "",
        outputDescription: "",
        inputTests: [],
        outputTests: []
      },{
        id: 7,
        title: "Square Country",
        text: "",
        inputDescription: "",
        outputDescription: "",
        inputTests: [],
        outputTests: []
      },{
        id: 8,
        title: "Very Short Problem",
        text: "",
        inputDescription: "",
        outputDescription: "",
        inputTests: [],
        outputTests: []
      },{
        id: 9,
        title: "Domino Puzzle",
        text: "",
        inputDescription: "",
        outputDescription: "",
        inputTests: [],
        outputTests: []
      },
      {
        id: 10,
        title: "Discrete Function",
        text: "",
        inputDescription: "",
        outputDescription: "",
        inputTests: [],
        outputTests: []
      },
    ];
    const contests: Contest[] = [
      { 
        id: 1,
        startTime: new Date(Date.now() - 198*60*1000),
        duration: 300,
        title: 'ACM ICPC World Finals 2018',
        problems: problems,
      },
    ];

    const submissions: Submission[] = [
      {
        problemId: 1,
        code:
`#define _CRT_SECURE_NO_DEPRECATE
#pragma comment(linker, "/STACK:128777216")

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <sstream>

#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include <bitset>
#include <queue>
#include <deque>
#include <stack>

#include <math.h>
#include <cmath>
#include <string>
#include <cstring>
#include <string.h>

#include <memory.h>
#include <cassert>
#include <time.h>
#include <functional>

using namespace std;

#define forn(i,n) for (int i = 0; i < (int)(n); i++)
#define fornd(i, n) for (int i = (int)(n) - 1; i >= 0; i--)
#define forab(i,a,b) for (int i = (int)(a); i <= (int)(b); i++)
#define forabd(i, b, a) for (int i = (int)(b); i >= (int)(a); i--)
#define forit(i, a) for (__typeof((a).begin()) i = (a).begin(); i != (a).end(); i++)

#define _(a, val) memset (a, val, sizeof (a))
#define sz(a) (int)((a).size())
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()

typedef long long lint;
typedef unsigned long long ull;
typedef long double ld;
typedef pair<int, int> pii;
typedef vector<int> vii;

const lint LINF = 1000000000000000000LL;
const int INF = 1000000000;
const long double eps = 1e-9;
const long double PI = 3.1415926535897932384626433832795l;

#ifdef MY_DEBUG
#define dbgx( x ) { cerr << #x << " = " << x << endl; }
#define dbg( ... ) { fprintf(stderr, __VA_ARGS__); fflush(stderr); }
#else
#define dbgx( x ) {  } 
#define dbg( ... ) {  } 
#endif

void prepare(string s) {
#ifdef MY_DEBUG
  freopen("input.txt", "r", stdin);
  //freopen ("output.txt", "w", stdout);
#else
  //freopen("input.txt", "r", stdin);
  //freopen ("output.txt", "w", stdout);
#endif
}

int n;
int a[105][2];
int b[105][2];

int g[105][105];
int mt[105];
int used[105];

bool dfs(int v) {
  if (used[v])
    return false;
  used[v] = 1;
  forn(i, n) {
  if (g[v][i]) {
    if (mt[i] == -1 || dfs(mt[i])) {
      mt[i] = v;
      return true;
    }
  }
  }
  return false;
}

void read() {
  scanf("%d", &n);
  forn(i, n) {
    scanf("%d %d", &a[i][0], &a[i][1]);
  }
  forn(i, n) {
    scanf("%d %d", &b[i][0], &b[i][1]);
  }
}

void solve() {
  forn(i, n) {
    forn(j, n) {
      if (a[i][0] < b[j][0] && a[i][1] < b[j][1]) {
        g[i][j] = 1;
      } else {
        g[i][j] = 0;
      }
    }
  }
  _(mt, -1);
  int ans = 0;
  forn(i, n) {
    _(used, 0);	  
    if (dfs(i)) {
  ans++;
    }
  }
  printf("%d\n", ans);
}

int main() {
  prepare("");

  read();
  solve();
  dbg("Clock = %.3f\n", clock() / (double)CLOCKS_PER_SEC);
  return 0;
}
        `,
        status: "AC",
        time: new Date(Date.now()),
        lang: "c++"
      },
      {
        problemId: 3,
        code: 
`import java.util.*;
import java.lang.*;
import java.io.*;

public class Main {
    public static void output (int[] a, int x, int y) {
        for (int i = x; i<=y; ++i) {
            System.out.print (a[i] + " ");
        }
        System.out.println ();
    }
    
    public static int GCD (int x, int y) {
        while (y > 0) {
            int tmp = x % y;
            x = y;
            y = tmp;
        }
        return x;
    }
  public static void main (String[] args) throws java.lang.Exception {
      Scanner in = new Scanner(System.in);
    int n = in.nextInt ();
    int[] a = new int [n+2];
    int[] b = new int [n+2];
    for (int i = 1; i<=n; ++i) a[i] = in.nextInt ();
    Arrays.sort (a, 1, n+1);
    int id = 0, rm = 0;
    for (int i = 2; i<=n; ++i) {
        b[++id] = a[i] - a[i-1];
        rm = GCD (b[id],rm);
    }
    int sum = 0;
    for (int i = 1; i<=id; ++i){
        sum += b[i]/rm - 1;
    }
    System.out.println (sum);
  }
}`,
        status: "WA",
        time: new Date(),
        lang: "java"
      }
    ];

    const teamNames: string[] = [
      "St. Petersburg National Research University of IT, Mechanics and Optics",
      "The University of Tokyo",
      "Shanghai Jiao Tong University",
      "National Taiwan University",
      "St. Petersburg State University",
      "University of Warsaw",
      "Taras Shevchenko Kiev National University",
      "Belarusian State University",
      "Jagiellonian University in Krakow",
      "Moscow State University",
      "Carnegie Mellon University",
      "Tsinghua University",
      "Perm State University",
      "Northeast Normal University",
      "Peking University",
      "Institut Teknologi Bandung",
      "University of Southern California",
      "Stanford University",
      "Fudan University",
      "University of Toronto",
      "University of Waterloo",
      "SungKyunKwan University",
      "Hong Kong University of Science and Technology",
      "University of Central Florida",
      "Cairo University - Faculty of Computers and Information",
      "Arab Academy for Science and Technology (Alexandria)"
    ];
    
    let rng = new RNG(12);
    
    const participants: Participant[] = [];
    for (let i = 0; i < teamNames.length; ++i) {
      let p: Participant = new Participant();
      p.id = i;
      p.nickname = teamNames[i];
      p.problems = [];
      for (let j = 0; j < problems.length; ++j) {
        let partProblem = new ParticipantProblem();
        partProblem.problemId = j + 1;
        if (rng.nextDouble() < 0.7) {
          partProblem.isSolved = true;
          partProblem.attempts = 1 + Math.floor(rng.nextDouble()*10);
          partProblem.lastAttemptTime = Math.floor(rng.nextDouble()*300);
        } else {
          partProblem.isSolved = false;
          if (rng.nextDouble() < 0.5) {
            partProblem.attempts = 0;
            partProblem.lastAttemptTime = 0;
          } else {
            partProblem.attempts = 1 + Math.floor(rng.nextDouble()*20);
            partProblem.lastAttemptTime = Math.floor(rng.nextDouble()*300);
          }
        }
        p.problems.push(partProblem);
      }
      participants.push(p);
    }
    participants[0].problems[1].isSolved = false;
    participants[0].problems[1].attempts = 0;
    participants[0].problems[6].isSolved = false;
    participants[0].problems[6].attempts = 0;

    return {'contests': contests, 'submissions': submissions, 'participants': participants};
  }
}
