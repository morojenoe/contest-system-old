import { ProblemDescription } from "./problem-description";

export class Contest {
  id: Number;
  title: string;
  startTime: Date;
  duration: number;
  problems: ProblemDescription[];
}
