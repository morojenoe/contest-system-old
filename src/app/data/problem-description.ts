import { ProblemTest } from "./problem-test";

export class ProblemDescription {
  id: Number;
  title: string;
  text: string;
  inputDescription: string;
  outputDescription: string;
  inputTests: ProblemTest[];
  outputTests: ProblemTest[];
}
