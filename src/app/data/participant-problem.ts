export class ParticipantProblem {
  problemId: number;
  isSolved: boolean;
  lastAttemptTime: number;
  attempts: number;
}
