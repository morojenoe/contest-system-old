export class Submission {
  code: string;
  lang: string;
  time: Date;
  status: string;
  problemId: Number;
}
