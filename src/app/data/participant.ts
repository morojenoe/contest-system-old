import { ParticipantProblem } from "./participant-problem";

export class Participant {
  id: Number;
  nickname: string;
  problems: ParticipantProblem[];
}
