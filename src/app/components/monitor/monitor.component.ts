import { Component, OnInit, Input } from '@angular/core';
import { Participant } from '../../data/participant';
import { ContestService } from '../../services/contest.service';
import { ProblemDescription } from '../../data/problem-description';

@Component({
  selector: 'app-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.css']
})
export class MonitorComponent implements OnInit {
  participants: Participant[];
  @Input() problems: ProblemDescription[];

  constructor(private contestService: ContestService) { }

  ngOnInit() {
    this.getParticipants();
  }

  getParticipants(): void {
    this.contestService.getParticipants().subscribe(participants => {
      this.participants = participants;
      this.sortParticipants();
    });
  }

  sortParticipants(): void {
    this.participants.sort((lft, rgh) => {
      let solvedLft = this.countSolvedProblems(lft);
      let solvedRgh = this.countSolvedProblems(rgh);
      if (solvedLft != solvedRgh) {
        return solvedRgh - solvedLft;
      }
      let penaltyLft = this.countPenaltyForACMFormat(lft);
      let penaltyRgh = this.countPenaltyForACMFormat(rgh);
      return penaltyLft - penaltyRgh;
    });
  }

  getProblemSymbols(): string[] {
    let result = [];
    for (let i = 0; i < this.problems.length; ++i) {
      let char: number = 'A'.charCodeAt(0);
      char += i;
      result.push(String.fromCharCode(char));
    }
    return result;
  }

  countSolvedProblems(participant: Participant): number {
    let solvedProblems = 0;
    for (let problem of participant.problems) {
      if (problem.isSolved) {
        ++solvedProblems;
      }
    }
    return solvedProblems;
  }

  countPenaltyForACMFormat(participant: Participant): number {
    let penalty = 0;
    for (let problem of participant.problems) {
      if (problem.isSolved) {
        penalty += problem.lastAttemptTime;
        penalty += (problem.attempts - 1) * 20;
      }
    }
    return penalty;
  }

  getTimeForStandings(minutes: number): Date {
    let date = new Date();
    date.setMinutes(minutes % 60);
    date.setHours(Math.floor(minutes / 60));
    return date;
  }
}
