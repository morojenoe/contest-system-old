import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { ProblemDescription } from '../../data/problem-description';
import { Submission } from '../../data/submission';
import { SubmissionsService } from '../../services/submissions.service';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-submissions',
  templateUrl: './submissions.component.html',
  styleUrls: ['./submissions.component.css']
})
export class SubmissionsComponent implements OnInit {
  @Input() problems: ProblemDescription[];

  submissions: Submission[];
  modalRef: BsModalRef;

  constructor(private submissionService: SubmissionsService,
              private modalService: BsModalService) { }

  ngOnInit() {
    this.getSubmissions();
  }

  getSubmissions(): void {
    this.submissionService.getSubmissions().subscribe(_submissions => this.submissions = _submissions);
  }

  getProblemTitle(id: Number): string {
    for (let p of this.problems) {
      if (p.id === id) {
        return p.id + ". " + p.title;
      }
    }
    return "No title found";
  }

  showSource(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
  }
}
