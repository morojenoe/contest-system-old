import { Component, OnInit, Input } from '@angular/core';
import { ProblemDescription } from '../../data/problem-description';
import { ProblemTest } from '../../data/problem-test';
import { Participant } from '../../data/participant';

@Component({
  selector: 'app-problem',
  templateUrl: './problem.component.html',
  styleUrls: ['./problem.component.css']
})
export class ProblemComponent implements OnInit {
  @Input() problems: ProblemDescription[];
  @Input() participant: Participant;
  activeProblem: ProblemDescription;

  constructor() { }

  ngOnInit() {
    this.activeProblem = this.problems[0];
  }

  changeActiveProblem(problem: ProblemDescription): void {
    this.activeProblem = problem;
  }

  concatenateTest(test: ProblemTest) {
    let result = "";
    for (let line of test.lines) {
      result += line;
      result += "\n";
    }
    return result;
  }

  problemTitleInProblemList(problem: ProblemDescription, i: number): string {
    return String.fromCharCode('A'.charCodeAt(0) + i) + ". " + problem.title;
  }

  isProblemSolved(problemId: number): boolean {
    if (this.participant == undefined)
      return false;
    let problem = this.participant.problems.find(problem => problem.problemId === problemId);
    if (problem === undefined)
      return false;
    return problem.isSolved;
  }

  isProblemAttempted(problemId: number): boolean {
    if (this.participant == undefined)
      return false;
    let problem = this.participant.problems.find(problem => problem.problemId === problemId);
    if (problem === undefined)
      return false;
    return !problem.isSolved && problem.attempts > 0;
  }
}
